#!/usr/bin/env python

# 2008-09-17.131915+0200CEST.txt:(13:27:34) wjt: mcepl: if the send_raw
# prpl method and the #xmpp-receiving-xml (possibly differently named)
# signal are exposed on the bus, yes
#
# There is JabberReceivingXmlnode
# (22:58:25) nosnilmot: mcepl: it's actually "jabber-receiving-xmlnode"
# and doesn't appear to be documented anywhere I can find, it appears to
# be emitted with two parameters: (PurpleConnection *gc, xmlnode **node)
# ("jabber-sending-xmlnode" is the # sending equivalent, emitted in the
# same way)
#
# https://developer.pidgin.im/wiki/DbusHowto
import dbus

import gobject


def my_func(account, sender, message, conversation, flags):
    print("%s said: %s" % (sender, message))


dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
bus = dbus.SessionBus()

bus.add_signal_receiver(my_func,
                        dbus_interface="im.pidgin.purple.PurpleInterface",
                        signal_name="ReceivedImMsg")

loop = gobject.MainLoop()
loop.run()
